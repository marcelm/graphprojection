Heuristic Transitive Graph Projection (Cluster Editing)
=======================================================

This software implements a heuristic algorithm to solve the *cluster editing* problem, also called *transitive graph projection*. It is a quality heuristic, that is, it does not necessarily find the optimal solution. The algorithm is described in the paper: “Exact and heuristic algorithms for weighted cluster editing”, see <http://www.ncbi.nlm.nih.gov/pubmed/17951842>.
The paper also describes a large class of graphs for which the algorithm does find the optimal solution.

I wrote the code years ago as part of my diploma thesis. I would do many things differently now. In the spirit of the Nature article “Publish your computer code: it is good enough“ (see <http://www.nature.com/news/2010/101013/full/467753a.html>), I am publishing it now (November 2013).


License
=======

(This is the MIT license.)

Copyright (c) 2006-2008,2013 Marcel Martin <marcel.martin@tu-dortmund.de>

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.


Dependencies
============

The software was developed with Python 2.4 and NetworkX 0.35.1 (a Python graph library). Python 2.7 should work, but the most recent NetworkX version does not. Create a virtualenv and then install that ancient NetworkX version with `pip install 'networkx==0.35.1'`.


Running
=======

This is how to run the program:

    python graphprojection.py inputfile1.cm [inputfile2.cm ...]

To compute the cost of a transitive graph projection for all example graphs that are included, run this command:

	python graphprojection.py -M 50 data/*.cm

The output should be the same as in the file `expected-result.txt`.


Input file format
=================

The input files must be “component matrix files”, which look as follows:

```
4
FIRST
SECOND
THIRD
FOURTH
1.44    9.5 2.2
-5.8    -0.6
-4.5
```

Line 1: Number of vertices in the graph (n in the following)
Lines 2 to n+1: Vertex names
Remaining lines: Edge costs. The first of those lines has n-1 entries, the second n-2 and so on until 1. Entries must be separated by tabs! In the example above, the edge costs are:

FIRST -- SECOND: 1.44
FIRST -- THIRD: 9.5
FIRST -- FOURTH: 2.2
SECOND -- THIRD: -5.8
SECOND -- FOURTH: -0.6
THIRD -- FOURTH: -4.5


C++ implementation
==================

The algorithm was also re-written in C++. That implementation can be found in the cpp/ subdirectory. To compile it, change into the directory and run `cmake .`, followed by `make`. Then run `./graphprojection ../data/*.cm`.

# kate: syntax markdown;
