#include <iostream>
#include <fstream>
#include <vector>
#include <string>
#include <ext/hash_map>
#include <ext/hash_set>
#include <queue>
#include <sstream>
#include <cassert>
#include <list>
#include <cmath>
#include <limits>

/*
- Kante existiert wenn w > 0.0
- delete_edge soll alpha als parameter bekommen statt neues gewicht
- path_exists kann man auch mit DFS oder BFS implementieren
*/

using __gnu_cxx::hash_map;
using __gnu_cxx::hash_set;
using std::vector;
using std::string;
using std::istringstream;

using namespace std;

const double MY_INFINITY = std::numeric_limits<double>::infinity();

struct simple_hash_func {
	size_t operator()(size_t x) const { return x; }
};

typedef __gnu_cxx::hash<size_t> hash_func;

//typedef hash_map<size_t,double,hash_func> edgemap_type;
typedef vector<size_t> edgemap_type;


struct Edge {
	Edge(size_t u=0, size_t v=0, double w=0.0)
		: u(u), v(v), w(w)
	{ }

	bool operator < (const Edge& other) const {
		if (u == other.u)
			return v < other.v;
		return u < other.u;
	}

	size_t u, v;
	double w;
};

/* an edge and its transitivity improvement */
struct EdgeScore {
	EdgeScore(size_t u=0, size_t v=0, double ti=0.0) :
		u(u), v(v), ti(ti)
	{ }
	size_t u, v;
	double ti;
	bool operator > (const EdgeScore& other) const { return ti > other.ti; }
	bool operator <= (const EdgeScore& other) const { return ti <= other.ti; }
};

ostream& operator << (ostream& os, const EdgeScore& es) {
	return os << "(" << es.u << "," << es.v << "):" << es.ti;
}

const Edge NOEDGE(0, 0, 0.0);
class Graph;
class DftGraphView;

class EdgeIterator {
	public:
		// set at_end to true if the iterator should be at the end
		EdgeIterator(const Graph& graph, bool at_end=false);

		const EdgeIterator& operator++();

		inline const Edge& operator* () const;

		const Edge* operator-> () const {
			return &(operator*());
		}

		bool operator == (const EdgeIterator& other) const;
		bool operator != (const EdgeIterator& other) const {
			return !(*this == other);
		}

	private:
		const Graph& graph;
		mutable Edge edge;

	protected:
		vector<edgemap_type>::const_iterator vertex_iterator;
		edgemap_type::const_iterator edge_iterator;
		size_t u, v;
};


/**
 * This works only when the DftGraphView is a connected component.
 */
class EdgeViewIterator {
	public:
		// set at_end to true if the iterator should be at the end
		EdgeViewIterator(const DftGraphView& view, bool at_end=false);

		const EdgeViewIterator& operator++();

		inline const Edge& operator* () const;

		const Edge* operator-> () const {
			return &(operator*());
		}

		bool operator == (const EdgeViewIterator& other) const;
		bool operator != (const EdgeViewIterator& other) const {
			return !(*this == other);
		}

	private:
		const DftGraphView& view;
		mutable Edge edge;

	protected:
		vector<size_t>::const_iterator visible_vertex_iterator;
		vector<edgemap_type>::const_iterator vertex_iterator;
		edgemap_type::const_iterator edge_iterator;
};

class Graph {
	public:
		typedef edgemap_type::const_iterator neighbors_iterator;
		typedef EdgeIterator edge_iterator;

		Graph(size_t n) : vertices(n), matrix(n), names(n), num_edges(0) {
			// only the lower triangle of the matrix is stored
			// on accessing matrix[u][v], it must be made
			// sure that v < u
			for (size_t i = 1; i < n; ++i) {
				matrix[i].assign(i, 0.0);
			}
		}

		/** Edge may or may not exist */
		void set_edge(size_t u, size_t v, double w) {
			//cerr << "set_edge(" << u << ", " << v << ", " << w << ")\n";
			assert(u != v);
			if (u < v) swap(u, v);

			if (matrix[u][v] <= 0.0) {
				if (w > 0.0) {
					// old doesnt exist, new does exist
					vertices[u].push_back(v);
					vertices[v].push_back(u);
					++num_edges;
				}
			} else {
				if (w <= 0.0) {
					// old does exist, new doesnt exist
					vertices[u].erase(find(vertices[u].begin(), vertices[u].end(), v));
					vertices[v].erase(find(vertices[v].begin(), vertices[v].end(), u));
					--num_edges;
				}
			}
			matrix[u][v] = w;
// 			cerr << "num_edges: " << num_edges << '\n';
		}

		/**
		 * Returns weight of edge (u, v). Note that the weight may be
		 * negative. */
		double get_weight(size_t u, size_t v) const {
			if (u < v) swap(u, v);
			return matrix[u][v];
		}

		bool has_edge(size_t u, size_t v) const {
			return get_weight(u, v) > 0.0;
		}

		size_t number_of_edges() const { return num_edges; }

		size_t number_of_vertices() const { return vertices.size(); }

		void set_vertex_name(size_t v, const std::string& name) {
			assert(v < names.size());
			names[v] = name;
		}

		vector<vector<size_t> > connected_components() const;
		size_t size() const { return vertices.size(); }
		edge_iterator edges_begin() const { return edge_iterator(*this); }
		edge_iterator edges_end() const { return edge_iterator(*this, true); }

		neighbors_iterator neighbors_begin(size_t v) const {
			return vertices[v].begin();
		}

		neighbors_iterator neighbors_end(size_t v) const {
			return vertices[v].end();
		}

		bool path_exists(size_t source, size_t target) const;

	protected:
		vector<edgemap_type> vertices;
		vector<vector<double> > matrix;
		vector<string> names;
		size_t num_edges;

	friend class EdgeIterator;
	friend class DfsVisitor;
	friend class EdgeViewIterator;
	friend class DftGraphView;
};

ostream& operator << (ostream& os, const Graph& graph);
void print_graph(const DftGraphView& graph);

inline EdgeIterator::EdgeIterator(const Graph& graph, bool at_end)
	: graph(graph)
	, edge(0, 0, 0.0)
	, vertex_iterator(graph.vertices.begin())
	, u(0)
	, v(0)
{
	while (vertex_iterator != graph.vertices.end() &&
			vertex_iterator->begin() == vertex_iterator->end())
	{
		++vertex_iterator;
		++u;
	}
	if (vertex_iterator != graph.vertices.end()) {
		edge_iterator = vertex_iterator->begin();
		v = 0;
	}
	if (at_end) {
		vertex_iterator = graph.vertices.end();
		u = graph.vertices.size();
	}
}

inline const Edge& EdgeIterator::operator* () const {
	edge = Edge(u, v, graph.get_weight(u, v));
	return edge;
}

inline const EdgeIterator& EdgeIterator::operator++() {
	assert(vertex_iterator != graph.vertices.end());

	++edge_iterator;
	++v;
	if (edge_iterator == vertex_iterator->end()) {
		++vertex_iterator;
		++u;
		while (vertex_iterator != graph.vertices.end() &&
				vertex_iterator->begin() == vertex_iterator->end()) {
			++vertex_iterator;
			++u;
		}
		if (vertex_iterator != graph.vertices.end()) {
			edge_iterator = vertex_iterator->begin();
			v = 0;
		}
	}
	return *this;
}

bool EdgeIterator::operator == (const EdgeIterator& other) const {
	// at end, only vertex_iterators need to be the same
	if (vertex_iterator == graph.vertices.end()) {
		return other.vertex_iterator == graph.vertices.end();
	} else {
		return vertex_iterator == other.vertex_iterator
			&& edge_iterator == other.edge_iterator;
	}
}


double min3(double x, double y, double z) {
	assert(x >= 0.0 and y >= 0.0 and z >= 0.0);
	double m = x;
	if (y < m) m = y;
	if (z < m) m = z;
	return m;
}


class EdgeHeap {
	public:
		EdgeHeap(size_t n) : edgesmap(n, vector<size_t>(n)) { }
		EdgeHeap(const EdgeHeap& eh, const vector<size_t>& visible_vertices);
		void add_edge(size_t u, size_t v, double ti) {
			add_edge(EdgeScore(u, v, ti));
		}

		void add_edge(EdgeScore es);


		EdgeScore pop_max_edge();

		EdgeScore get_max_edge() const {
			assert(edgesheap.size() > 0);
// 			cerr << "get_max_edge: " << edgesheap[0] << endl;
			return edgesheap[0];
		}

		void modify_edge(size_t u, size_t v, double tidiff) {
			(void)tidiff;
			exit(1);
			if (u < v) swap(u, v);
			assert(false);

		// if prev. score is < ti
		}

		inline void increase_edge(size_t u, size_t v, double tidiff);
		void decrease_edge(size_t u, size_t v, double tidiff);

	private:
		inline void swap_edges(size_t i, size_t j);
		void bubble_down(size_t);
		void bubble_up(size_t);

		// this vector will be organized as a heap
		vector<EdgeScore> edgesheap;

		// maps edges to heap indices
		vector<vector<size_t> > edgesmap; // TODO this could be a reference
#ifndef NDEBUG
		bool check_invariant() const;
#endif
		bool is_max_heap() const;

	friend ostream& operator << (ostream& os, const EdgeHeap& heap);
};

EdgeHeap::EdgeHeap(const EdgeHeap& eh, const vector<size_t>& visible_vertices)
	: edgesmap(eh.edgesmap.size(), vector<size_t>(eh.edgesmap.size()))
{
	// copy from eh.edgesheap into this->edgesheap if
	// one of the vertices is visible

	// create a temporary hash with all the visible vertices
	hash_set<size_t> tmp;
	tmp.insert(visible_vertices.begin(), visible_vertices.end());

	vector<EdgeScore>::const_iterator from;
	for (from = eh.edgesheap.begin(); from != eh.edgesheap.end(); ++from) {
		if (tmp.count(from->u) > 0) {
			add_edge(*from);
		}
	}
#ifndef NDEBUG
	check_invariant();
#endif
}

ostream& operator << (ostream& os, const EdgeHeap& heap) {
	copy(heap.edgesheap.begin(), heap.edgesheap.end(), ostream_iterator<EdgeScore>(os, " "));
	return os;
}

// swaps edges with index i and j
// use this to keep edgesheap and edgesmap in sync
inline void EdgeHeap::swap_edges(size_t i, size_t j) {
	assert(i < edgesheap.size() and j < edgesheap.size());
	swap(edgesheap[i], edgesheap[j]);

	edgesmap[edgesheap[i].u][edgesheap[i].v] = i;
	edgesmap[edgesheap[j].u][edgesheap[j].v] = j;
}


/**
 * Swaps this edge with the appropriate child as long as
 * the heap property is violated.
*/
void EdgeHeap::bubble_down(size_t parent) {
	size_t child = 2*parent + 2;
	size_t n = edgesheap.size();
	while (child < n) {
		// pick the larger of both children
		if (edgesheap[child-1] > edgesheap[child]) {
			--child;
		}
		// swap
		if (edgesheap[parent] > edgesheap[child]) {
			break;
		}
		swap_edges(parent, child);
		parent = child;
		// right child
		child = 2*child+2;
	}
	if (child == n and edgesheap[child-1] > edgesheap[parent]) {
		swap_edges(parent, child-1);
	}
}

void EdgeHeap::bubble_up(size_t child) {
	size_t parent = (child-1) / 2;
	while (child > 0 and edgesheap[child] > edgesheap[parent]) {
		swap_edges(child, parent);
		child = parent;
		parent = (child-1) / 2;
	}
}

// some ideas taken from the GNU STL
EdgeScore EdgeHeap::pop_max_edge() {
#ifndef NDEBUG
	check_invariant();
#endif
	EdgeScore maxedge = edgesheap.front();
	swap_edges(0, edgesheap.size() - 1);
	edgesheap.pop_back();

	bubble_down(0);
#ifndef NDEBUG
	check_invariant();
#endif
	return maxedge;
}

// TODO This is probably not linear when used to
// build a heap from scratch.
void EdgeHeap::add_edge(EdgeScore es) {
	//cerr << "add_edge(" << u << ", " << v << ", " << ti << ")\n";
	//cerr << "heap is: ";
	//copy(edgesheap.begin(), edgesheap.end(), ostream_iterator<EdgeScore>(cerr, " "));
	//cerr << '\n';
#ifndef NDEBUG
	check_invariant();
#endif
	if (es.u < es.v) swap(es.u, es.v);

#ifndef NDEBUG
	for (vector<EdgeScore>::iterator it = edgesheap.begin(); it != edgesheap.end(); ++it) {
		assert(not (it->u == es.u and it->v == es.v));
	}
#endif
	edgesheap.push_back(es);

	size_t child = edgesheap.size() - 1;
	edgesmap[es.u][es.v] = child;

	bubble_up(child);
#ifndef NDEBUG
	check_invariant();
#endif
}


inline void EdgeHeap::increase_edge(size_t u, size_t v, double tidiff) {
	assert(tidiff >= 0.0);
	if (u < v) swap(u, v);
	size_t index = edgesmap[u][v];
	edgesheap[index].ti += tidiff;
	bubble_up(index);
}

// TODO modify this so it accepts only negative tidiff values
void EdgeHeap::decrease_edge(size_t u, size_t v, double tidiff) {
// 	cerr << "tidiff: " << tidiff << endl;
	assert(tidiff >= 0.0);
	if (u < v) swap(u, v);
	size_t index = edgesmap[u][v];
	edgesheap[index].ti -= tidiff;
	bubble_down(index);

}

#ifndef NDEBUG
// checks whether class invariants are fulfilled
bool EdgeHeap::check_invariant() const {
	assert(is_max_heap());
	return true;
}
#endif

// for debugging purposes: checks whether the vector is a heap
// algorithm taken from the GNU STL
bool EdgeHeap::is_max_heap() const {
	size_t parent = 0;
	for (size_t child = 1; child < edgesheap.size(); ++child) {
		if (edgesheap[child] > edgesheap[parent])
			return false;
		if (child & 1 == 0)
			++parent;
	}
	return true;
}


//TODO common neighbors optimization (if #(common) > 0, then the graph surely is still connected)

class DftGraphView {
	public:
		typedef vector<size_t>::const_iterator vertex_iterator;
		typedef EdgeViewIterator edge_iterator;

		DftGraphView(Graph& graph, vector<size_t>& visible_vertices)
			: graph(graph)
			, visible_vertices(visible_vertices)
			, improvements(graph.size())
		{
			compute_all_improvements();
		}

		DftGraphView(DftGraphView& graphview, vector<size_t>& visible_vertices)
			: graph(graphview.graph)
			, visible_vertices(visible_vertices)
			, improvements(graphview.improvements, visible_vertices)
		{
		}

		/** This constructor makes all vertices visible */
		DftGraphView(Graph& graph)
			: graph(graph)
			, visible_vertices(graph.size())
			, improvements(graph.size())
		{
			for (size_t i = 0; i < graph.size(); ++i)
				visible_vertices[i] = i;
			compute_all_improvements();
		}

		/** Returns the transitivity improvement of the edge with the
		 * highest transitivity improvement and deletes it. */
		double delete_culprit();

		Edge get_culprit() const {
			EdgeScore es = improvements.get_max_edge();
			return Edge(es.u, es.v, graph.get_weight(es.u, es.v));
		}

		double get_culprit_improvement() const {
			EdgeScore es = improvements.get_max_edge();
			return es.ti;
		}

		void add_edge(size_t u, size_t v, double w);

		size_t number_of_vertices() const { return visible_vertices.size(); }

		double get_weight(size_t u, size_t v) const { return graph.get_weight(u, v); }

		vertex_iterator vertices_begin() const { return visible_vertices.begin(); }
		vertex_iterator vertices_end() const { return visible_vertices.end(); }
		edge_iterator edges_begin() const { return edge_iterator(*this); }
		edge_iterator edges_end() const { return edge_iterator(*this, true); }

		vector<vector<size_t> > connected_components() const;
		bool path_exists(size_t source, size_t target) const {
			return graph.path_exists(source, target);
		}

	private:
		double compute_improvement(size_t u, size_t v, double w) const;

		/**
		 * Computes TI for all edges in the graph and
		 * stores TI values for later. Only used by the constructor. */
		void compute_all_improvements() {
			// iterate over all edges, compute transitivity improvement,
			// and store it
			edge_iterator eit = edges_begin();
			edge_iterator eit_end = edges_end();
			for ( ; eit != eit_end; ++eit) {
				// TODO this should be done in the edge_iterator
				if (eit->u < eit->v)
					continue;
				double ti = compute_improvement(eit->u, eit->v, eit->w); // TODO or just *eit?
				improvements.add_edge(eit->u, eit->v, ti);
			}
		}

	private:
		Graph& graph;
		vector<size_t> visible_vertices; // TODO reference?
		EdgeHeap improvements;
		size_t num_edges;

	friend class EdgeViewIterator;
	friend class DfsVisitor;
};


inline EdgeViewIterator::EdgeViewIterator(const DftGraphView& view, bool at_end)
	: view(view)
	, edge(0, 0, 0.0)
	, visible_vertex_iterator(view.visible_vertices.begin())
{
	if (at_end) {
		visible_vertex_iterator = view.visible_vertices.end();
		return;
	}
	while (visible_vertex_iterator != view.visible_vertices.end()) {
		vertex_iterator = view.graph.vertices.begin() + *visible_vertex_iterator;
		if (!vertex_iterator->empty()) {
			edge_iterator = vertex_iterator->begin();
			break;
		}
		++visible_vertex_iterator;
	}
	// note that edge_iterator is undefined if no edges exist
}


inline const Edge& EdgeViewIterator::operator* () const {
	edge = Edge(*visible_vertex_iterator, *edge_iterator, view.get_weight(*visible_vertex_iterator, *edge_iterator));
	return edge;
}

/**
 * variables:
 * - visible_vertex_iterator iterates over visible_vertices
 * - vertex_iterator is always vertices[*visible_vertex_iterator]
 * - edge_iterator iterates over *vertex_iterator
 */
inline const EdgeViewIterator& EdgeViewIterator::operator++() {
	assert(visible_vertex_iterator != view.visible_vertices.end());

	++edge_iterator;
	if (edge_iterator == vertex_iterator->end()) {
		++visible_vertex_iterator;
		while (visible_vertex_iterator != view.visible_vertices.end()) {
			vertex_iterator = view.graph.vertices.begin() + *visible_vertex_iterator;
			if (!vertex_iterator->empty()) {
				edge_iterator = vertex_iterator->begin();
				break;
			}
			++visible_vertex_iterator;
		}
	}
	return *this;
}


bool EdgeViewIterator::operator == (const EdgeViewIterator& other) const {
	// at end, only visible_vertex_iterators need to be the same
	if (visible_vertex_iterator == view.visible_vertices.end()) {
		return other.visible_vertex_iterator == view.visible_vertices.end();
	} else {
		return vertex_iterator == other.vertex_iterator
			&& edge_iterator == other.edge_iterator;
	}
}

class DfsVisitor {
	public:
		DfsVisitor(const Graph& graph, vector<bool>& visited, vector<size_t>& component) :
			graph(graph)
			,visited(visited)
			,component(component)
		{
		}

		DfsVisitor(const DftGraphView& view, vector<bool>& visited, vector<size_t>& component) :
			graph(view.graph)
			,visited(visited)
			,component(component)
		{
		}

		void visit(size_t v) {
			visited[v] = true;
			component.push_back(v);
			edgemap_type::const_iterator hit = graph.vertices[v].begin();
			for ( ; hit != graph.vertices[v].end(); ++hit) {
				if (!visited[*hit]) {
					visit(*hit);
				}
			}
		}

	private:
		const Graph& graph;
		vector<bool>& visited;
		vector<size_t>& component;
};

vector<vector<size_t> > Graph::connected_components() const {
	vector<bool> visited(vertices.size(), false);
	vector<vector<size_t> > components;

	for (size_t u = 0; u < vertices.size(); ++u) {
		if (!visited[u]) {
			components.push_back(vector<size_t>());
			vector<size_t>& component = components.back();
			DfsVisitor(*this, visited, component).visit(u);
		}
	}
	return components;
}

vector<vector<size_t> > DftGraphView::connected_components() const {
	vector<bool> visited(graph.size(), false);
	vector<vector<size_t> > components;

	vector<size_t>::const_iterator uit = visible_vertices.begin();
	for ( ; uit != visible_vertices.end(); ++uit) {
		if (!visited[*uit]) {
			components.push_back(vector<size_t>());
			vector<size_t>& component = components.back();
			DfsVisitor(*this, visited, component).visit(*uit);
		}
	}
	return components;
}


/**
 * Computes transitivity improvement of edge (u,v),
 * assuming it has weight w. */
double DftGraphView::compute_improvement(size_t u, size_t v, double w) const {
	double ti = 0.0;

	const vector<edgemap_type>& vertices = graph.vertices;
	// mark all neighbors of u
	vector<bool> seen(vertices.size(), false);
	edgemap_type::const_iterator neighbor_it = vertices[u].begin();
	for ( ; neighbor_it != vertices[u].end(); ++neighbor_it) {
		seen[*neighbor_it] = true;
	}

	// iterate over v's neighbors and see whether they
	// are common or noncommon neighbors
	for (neighbor_it = vertices[v].begin();
			neighbor_it != vertices[v].end(); ++neighbor_it) {
		if (*neighbor_it != u) {
			// if we've seen it before, this is a common neighbor
			if (seen[*neighbor_it]) {
				ti -= min3(w, get_weight(u, *neighbor_it), get_weight(v, *neighbor_it));
				seen[*neighbor_it] = false;
			} else {
				// noncommon neighbor of v
				ti += min3(w, -get_weight(*neighbor_it, u), get_weight(v, *neighbor_it));
			}
		}
	}

	// iterate over neighbors of u to collects its neighbors
	// which are noncommon
	for (neighbor_it = vertices[u].begin();
			neighbor_it != vertices[u].end(); ++neighbor_it) {
		if (seen[*neighbor_it] && *neighbor_it != v) {
			// noncommon neighbor of u
			ti += min3(w, -get_weight(*neighbor_it, v), get_weight(u, *neighbor_it));
		}
	}
	return ti - w;
}

/**
 * Deletes the edge with the highest ti score
 * from the graph and updates the cached ti scores
 * for the surrounding edges.
 */
double DftGraphView::delete_culprit() { // TODO double factor=-1.0
	//cerr << "delete_culprit BEGIN" << endl;
	//cerr << "  heap: " << improvements << endl;

	EdgeScore es = improvements.pop_max_edge();
	double ti = es.ti;
	size_t u = es.u;
	size_t v = es.v;
	double w = -get_weight(u, v); // TODO factor * ...

	assert(w <= 0.0);
	graph.set_edge(u, v, w);

	const vector<edgemap_type>& vertices = graph.vertices;

	// recompute TI values for edges to adjacent nodes

	// mark all neighbors of u
	vector<bool> seen(vertices.size(), false);
	edgemap_type::const_iterator neighbor_it = vertices[u].begin();
	for ( ; neighbor_it != vertices[u].end(); ++neighbor_it) {
		seen[*neighbor_it] = true;
	}

	// iterate over v's neighbors and see whether they
	// are common or noncommon neighbors
	for (neighbor_it = vertices[v].begin();
			neighbor_it != vertices[v].end(); ++neighbor_it) {
		const size_t x = *neighbor_it;
		if (x == u)  // wenn einem hier kein X fuer ein U vorgemacht wird ...
			continue;
		// if we've seen it before, this is a common neighbor
		if (seen[x]) {
			double d = min3(-w, get_weight(u, x), get_weight(v, x));
			improvements.increase_edge(u, x, 2*d);
			improvements.increase_edge(v, x, 2*d);
			seen[x] = false;
		} else {
			// noncommon neighbor of v
			improvements.decrease_edge(v, x, min3(-w, -get_weight(x, u), get_weight(v, x)));
		}
	}

	// iterate over neighbors of u to collects its neighbors
	// which are noncommon
	for (neighbor_it = vertices[u].begin();
			neighbor_it != vertices[u].end(); ++neighbor_it) {
		const size_t x = *neighbor_it;
		if (seen[x] && x != v) {
			// noncommon neighbor of u
			improvements.decrease_edge(u, x, min3(-w, -get_weight(x, v), get_weight(u, x)));
		}
	}
	//cerr << "  heap: " << improvements << endl;
	//cerr << "delete_culprit END\n";
	return ti;
}

void DftGraphView::add_edge(size_t u, size_t v, double w) {
	assert(w > 0.0);
	graph.set_edge(u, v, w);

//TODO	assert(!improvements.has_edge(u, v));
	improvements.add_edge(u, v, compute_improvement(u, v, w));

	const vector<edgemap_type>& vertices = graph.vertices;
	// recompute TI values for edges to adjacent nodes

	// mark all neighbors of u
	vector<bool> seen(vertices.size(), false);
	edgemap_type::const_iterator neighbor_it = vertices[u].begin();
	for ( ; neighbor_it != vertices[u].end(); ++neighbor_it) {
		seen[*neighbor_it] = true;
	}

	// iterate over v's neighbors and see whether they
	// are common or noncommon neighbors
	for (neighbor_it = vertices[v].begin();
			neighbor_it != vertices[v].end(); ++neighbor_it) {
		const size_t& x = *neighbor_it;
		if (x == u) // wenn einem hier kein X fuer ein U vorgemacht wird ...
			continue;

		// if we've seen it before, this is a common neighbor
		if (seen[x]) {
			double d = min3(w, get_weight(u, x), get_weight(v, x));
			improvements.decrease_edge(u, x, 2*d);
			improvements.decrease_edge(v, x, 2*d);
			seen[x] = false;
		} else {
			// noncommon neighbor of v
			double d = min3(w, -get_weight(x, u), get_weight(v, x));
			improvements.increase_edge(v, x, d);
		}
	}

	// iterate over neighbors of u to collect those
	// which are noncommon
	for (neighbor_it = vertices[u].begin();
			neighbor_it != vertices[u].end(); ++neighbor_it) {
		size_t x = *neighbor_it;
		if (seen[x] && x != v) {
			// noncommon neighbor of u
			improvements.increase_edge(u, x, min3(w, -get_weight(x, v), get_weight(u, x)));
		}
	}
}

/**
 * Returns cost for a transitive closure. Assumes that
 * the graph is connected! */
double weighted_closure_cost_connected(const Graph& graph) {
	double cost = 0.0;
	for (size_t u = 0; u < graph.size(); ++u) {
		for (size_t v = 0; v < u; ++v) {
			if (u != v) {
				double w = graph.get_weight(u, v);
				if (w < 0.0) {
					cost -= w;
				}
			}
		}
	}

	return cost;
}

/**
 * Returns cost for a transitive closure. Assumes that
 * the graph is connected! */
double weighted_closure_cost_connected(const DftGraphView& view) {
	double cost = 0.0;
	DftGraphView::vertex_iterator v;
	for (v = view.vertices_begin(); v != view.vertices_end(); ++v) {
		DftGraphView::vertex_iterator u;
		for (u = view.vertices_begin(); u != v; ++u) {
			if (u != v) {
				double w = view.get_weight(*u, *v);
				if (w < 0.0) {
					cost -= w;
				}
			}
		}
	}

	return cost;
}

/**
 * Returns whether a path exists between u and v.
 * Currently, this routine uses Dijkstra's algorithm.
*/
bool Graph::path_exists(size_t source, size_t target) const {
	enum {
		WHITE, // (not visited)
		GRAY,  // (being visited)
		BLACK  // (visited)
	};
	vector<short> colors(size(), WHITE);
	queue<size_t,list<size_t> > grays; // TODO list-Parameter mal weglassen

	grays.push(source);
	colors[source] = GRAY;

	while (!grays.empty()) {
		size_t current = grays.front();
		grays.pop();

		// iterate over current's neighbors
		neighbors_iterator it;
		for (it = neighbors_begin(current); it != neighbors_end(current); ++it) {
			size_t x = *it;
			if (colors[x] == WHITE) {
				grays.push(x);
				colors[x] = GRAY;
			}
			if (x == target)
				return true;
		}
		colors[current] = BLACK;
	}
	return false;
}

/**
 * Input graph must be connected
 *
 * Returns a tuple of list of edges to delete and the cost to do it
 */
pair<list<Edge>, double>
transitive_projection_connected(DftGraphView& graph, double maxcost=MY_INFINITY)
{
	if (graph.number_of_vertices() <= 2) {
		return make_pair(list<Edge>(), 0.0);
	}
	list<Edge> deletions;
	double delcost = 0.0;
	double cost = weighted_closure_cost_connected(graph);
	maxcost = min(maxcost, cost);
	if (maxcost <= 0.0) {
		return make_pair(list<Edge>(), 0.0);
	}
	Edge culprit;
	do {
		culprit = graph.get_culprit();
		delcost += culprit.w;
		//cerr << "culprit: " << culprit.u << "," <<culprit.v<<":"<<culprit.w<<" at " << graph.get_culprit_improvement() << endl;
		assert(culprit.w > 0.0);
		//double ti = graph.get_culprit_improvement();
		graph.delete_culprit();
		deletions.push_back(culprit);

/* TODO		if cc > 0:
 			 cc tells us how many common neighbors u and v have.
 			 If they still have a common neighbor, then the current
 			 deletion can't lead to a split of the graph.
 			continue
*/
	} while (graph.path_exists(culprit.u, culprit.v));
// 	cerr << "split\n";
	vector<vector<size_t> > components = graph.connected_components();
	assert(components.size() == 2);

	// fix list of deletions
	hash_set<size_t> nodes1;
	hash_set<size_t> nodes2;

	nodes1.insert(components[0].begin(), components[0].end());
	nodes2.insert(components[1].begin(), components[1].end());

	list<Edge>::iterator dit = deletions.begin();
	while (dit != deletions.end()) {
		size_t u, v;
		u = dit->u;
		v = dit->v;
		if ((nodes1.count(u) > 0 and nodes1.count(v) > 0) or
			(nodes2.count(u) > 0 and nodes2.count(v) > 0))
		{
			delcost -= dit->w;
			// re-add edge
			graph.add_edge(u, v, dit->w);
			// delete it from list
			list<Edge>::iterator tmp = dit;
			++dit;
			deletions.erase(tmp);
		} else {
			++dit;
		}
	}
	if (delcost >= maxcost) {
		return make_pair(list<Edge>(), cost);
	}

	DftGraphView subgraphs[2] = {
		DftGraphView(graph, components[0]),
		DftGraphView(graph, components[1])
	};

	// recursively solve the problem for the two subgraphs
	pair<list<Edge>, double> result1 = transitive_projection_connected(subgraphs[0], maxcost - delcost);
	if (result1.second + delcost >= cost) {
		return make_pair(list<Edge>(), cost);
	}
	pair<list<Edge>, double> result2 = transitive_projection_connected(subgraphs[1], maxcost - delcost);

	double splitcost = result1.second + result2.second + delcost;
	if (splitcost < cost) {
		deletions.splice(deletions.end(), result1.first);
		deletions.splice(deletions.end(), result2.first);
		return make_pair(deletions, splitcost);
	}
	else {
		return make_pair(list<Edge>(), cost);
	}
}

/*
def postprocess(graph, deletions, cost):
	Returns corrected pair (deletions, cost).
	graph is not modified."""
	tmp = graph.copy()
	tmp.delete_edges_from(deletions)
	singletons = set(component[0] for component in nx.connected_components(tmp) if len(component) == 1)
	del tmp
	adjusted_deletions = []
	for u, v in deletions:
		if u in singletons and v in singletons:
			cost -= graph.get_weight(u, v)
			singletons.discard(u)
			singletons.discard(v)
		else:
			adjusted_deletions.append( (u, v) )
	return (adjusted_deletions, cost)
*/

/**
 * Computes a solution for transitive projection using the weighted
 * transitivity improvement heuristic. Returns list of
 * edge deletions and the cost as a pair. */
pair<list<Edge>,double> transitive_projection(Graph& graph, bool postprocessing=false) {
// 	cerr << "transitive_projection\n";
	list<Edge> deletions;
	double cost = 0.0;
	DftGraphView dftgraph(graph);
	vector<vector<size_t> > components = graph.connected_components();
	vector<vector<size_t> >::iterator it;

	for (it = components.begin(); it != components.end(); ++it) {
		DftGraphView graphview(dftgraph, *it);
		pair<list<Edge>, double> result = transitive_projection_connected(graphview);
		/*
		if (postprocessing) {
			result = postprocess(graphview, result.first, result.second);
		}
		*/
		deletions.splice(deletions.end(), result.first);
		cost += result.second;
	}

	return make_pair(deletions, cost);
}

class GraphReadError { };
using namespace std;

/** Read a graph in .cm format */
Graph read_cm(char* filename) {
	std::ifstream ifs(filename);

	// first line contains number of vertices
	string line;
	if (!getline(ifs, line)) {
		throw GraphReadError();
	}
	istringstream iss(line);
	size_t vertex_count;
	if (!(iss >> vertex_count)) {
		throw GraphReadError();
	}
	Graph graph(vertex_count);

	// read vertex names
	for (size_t i = 0; i < vertex_count; ++i) {
		if (!getline(ifs, line)) {
			throw GraphReadError();
		}
		graph.set_vertex_name(i, line);
	}

	// read edges
	for (size_t i = 0; i < vertex_count-1; ++i) {
		if (!getline(ifs, line)) {
			throw GraphReadError();
		}
		// split the line into vertex_count-i-1 double values
		istringstream iss(line);
		double w;
		for (size_t j = 0; j < vertex_count - i - 1; ++j) {
			if (!(iss >> w)) {
				throw GraphReadError();
			}
			graph.set_edge(i, i+j+1, w);
		}
	}
	return graph;
}


using namespace std;

ostream& operator << (ostream& os, const Graph& graph) {
	Graph::edge_iterator eit = graph.edges_begin();
	for ( ; eit != graph.edges_end(); ++eit) {
		os << "edge (" << eit->u << ", " << eit->v << "): " << eit->w << endl;
	}
	return os;
}

void print_graph(const DftGraphView& graph) {
	DftGraphView::edge_iterator eit = graph.edges_begin();
	for ( ; eit != graph.edges_end(); ++eit) {
		cout << "edge (" << eit->u << ", " << eit->v << "): " << eit->w << endl;
	}
}

double costsum(list<Edge> edges) {
	double cost = 0.0;
	for (list<Edge>::const_iterator it = edges.begin(); it != edges.end(); ++it) {
		cost += it->w;
	}
	return cost;
}

ostream& operator << (ostream& os, const list<Edge>& edgelist) {
	// normalize list, copy into vector
	vector<Edge> edges;
	list<Edge>::const_iterator it;
	for (it = edgelist.begin(); it != edgelist.end(); ++it) {
		size_t u = it->u;
		size_t v = it->v;
		if (u > v) swap(u, v);
		edges.push_back(Edge(u, v, it->w));
	}
	sort(edges.begin(), edges.end());
	vector<Edge>::const_iterator cit;
	os << '[';
	bool first = true;
	for (cit = edges.begin(); cit != edges.end(); ++cit) {
		if (!first) {
			os << ", ";
		} else {
			first = false;
		}
		os << '(' << cit->u << ", " << cit->v << ')';
	}
	os << ']';
	return os;
}

int main(int argc, char* argv[]) {
	cout << "# <component no.>\t<n>\t<m>\t<heuristic cost>\n";
	double total = 0.0;
	for (int i = 1; i < argc; ++i) {
// 		cerr << "reading graph no. " << i << " (" << argv[i] << ")\n";
// 		cerr << "name: " << argv[i] << endl;
		Graph g = read_cm(argv[i]);
// 		cerr << g << endl;//print_graph(g); // DEBUG
		cout << (i-1) << '\t' << g.number_of_vertices() << '\t' << g.number_of_edges() << '\t';
		cout.flush();
		pair<list<Edge>, double> result = transitive_projection(g);
		assert(abs(result.second - costsum(result.first)) < 1e4);
		cout << result.second << '\t'
			// << result.first
			<< endl;
		total += result.second;
	}
	cout << "# total cost: " << total << endl;

	return 0;
}
